/*
 * adc.c
 *
 *  Created on: 12 ���. 2014 �.
 *      Author: 1_admin_1
 */

#define TARGET_IS_BLIZZARD_RB1

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "utils/ustdlib.h"
#include "inc/hw_adc.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"


#include "driverlib/cpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "driverlib/udma.h"
#include "driverlib/fpu.h"
#include "driverlib/pin_map.h"
//#include "driverlib/uart.h"
#include "utils/uartstdio.h"

#include "driverlib/rom.h"
#include "adcudma.h"


#pragma DATA_ALIGN(ucControlTable, 1024)
unsigned char ucControlTable[1024];

unsigned short g_ulADCValues[UDMA_BUFFER_SIZE];
volatile unsigned char g_ucDataReady;
static volatile unsigned long g_uluDMAErrCount = 0;
static volatile unsigned long g_ulBadPeriphIsr1=0;
static volatile unsigned long g_ulBadPeriphIsr2=0;
static volatile bool readySW=false;



void ADC3IntHandler(void)
{

    unsigned long ulMode;
    unsigned int blockReady;


    blockReady=0;

    ADCIntClear(ADC0_BASE, ADC_SEQUENCER);


    ulMode = uDMAChannelModeGet(17 | UDMA_PRI_SELECT);
    if(ulMode == UDMA_MODE_STOP )
     {
	 blockReady++;
		uDMAChannelTransferSet(UDMA_CHANNEL_ADC3 | UDMA_PRI_SELECT,
				UDMA_MODE_PINGPONG,
							   (void *)(ADC0_BASE + ADC_O_SSFIFO3),
							   g_ulADCValues+UDMA_SIZE*uluDMACount, UDMA_SIZE);

     }
    ulMode = uDMAChannelModeGet(17 | UDMA_ALT_SELECT);

 	 if(ulMode == UDMA_MODE_STOP )
     {
	 blockReady++;
		uDMAChannelTransferSet(UDMA_CHANNEL_ADC3 | UDMA_ALT_SELECT,
				UDMA_MODE_PINGPONG,
							   (void *)(ADC0_BASE + ADC_O_SSFIFO3),
							   g_ulADCValues+UDMA_SIZE*uluDMACount, UDMA_SIZE);
     }


	 if(blockReady==1)
	 {
		uluDMACount++;
	 }
	 if ((uluDMACount)>=UDMA_BLOCKS)
		 uluDMACount=1;

	if (uluDMACount==4)
	{
		if (readySW)
			readyBlocks=0;
		readySW=false;
	}
	else
		readyBlocks=uluDMACount;

	 if (uluDMACount==3)
	 {

		ROM_uDMAChannelRequest(UDMA_CHANNEL_SW);

	 }
}


void InitADC3Transfer(void)
{
    unsigned int uIdx;
    uluDMACount=2;
    g_ucDataReady = 0;
    readyBlocks=0;

    for(uIdx = 0; uIdx < UDMA_BUFFER_SIZE; uIdx++)
    {
    	g_ulADCValues[uIdx] = 0;
    }

    //
	// Configure and enable the uDMA controller
	//
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);
	IntEnable(INT_UDMAERR);
	uDMAEnable();
	uDMAControlBaseSet(ucControlTable);

    //
    // Configure the ADC
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    SysCtlPeripheralReset(SYSCTL_PERIPH_ADC0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_2);
    ADCClockConfigSet(ADC0_BASE,  ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_EIGHTH,0);
    ADCSequenceConfigure(ADC0_BASE, ADC_SEQUENCER, ADC_TRIGGER_ALWAYS, 3);
    ADCSequenceStepConfigure(ADC0_BASE, ADC_SEQUENCER, 0, ADC_CTL_CH0 |
    							 ADC_CTL_END|ADC_CTL_IE);

    //
    // Enable the sequencer
    //
    ADCHardwareOversampleConfigure(ADC0_BASE, 4);
    ADCSequenceEnable(ADC0_BASE, ADC_SEQUENCER);
    ADCIntEnable(ADC0_BASE, ADC_SEQUENCER);

    uDMAChannelAttributeDisable(17, UDMA_ATTR_ALL );
    uDMAChannelControlSet(UDMA_CHANNEL_ADC3 | UDMA_PRI_SELECT,
						  UDMA_SIZE_16 | UDMA_SRC_INC_NONE |
						  UDMA_DST_INC_16 | UDMA_ARB_1);
    uDMAChannelControlSet(UDMA_CHANNEL_ADC3 | UDMA_ALT_SELECT,
						  UDMA_SIZE_16 | UDMA_SRC_INC_NONE |
						  UDMA_DST_INC_16 | UDMA_ARB_1);
    //
    // Configure the DMA channel
    //


		uDMAChannelTransferSet(UDMA_CHANNEL_ADC3 | UDMA_PRI_SELECT,
				UDMA_MODE_PINGPONG,
							   (void *)(ADC0_BASE + ADC_O_SSFIFO3),
							   g_ulADCValues, UDMA_SIZE);

		uDMAChannelTransferSet(UDMA_CHANNEL_ADC3 | UDMA_ALT_SELECT,
				UDMA_MODE_PINGPONG,
							   (void *)(ADC0_BASE + ADC_O_SSFIFO3),
							   g_ulADCValues+UDMA_SIZE, UDMA_SIZE);

    uDMAChannelEnable(UDMA_CHANNEL_ADC3);
}

void uDMAIntHandler(void) {
	unsigned short	ulMode = ROM_uDMAChannelModeGet(UDMA_CHANNEL_SW);
	if(ulMode == UDMA_MODE_STOP)
	{
		readySW=true;
	}
}

void uDMAErrorIntHandler(void)
{
	SysCtlDelay(10000);
}

void InitSWTransfer(void) {

	ROM_IntEnable(INT_UDMA);

    uDMAChannelAttributeDisable(UDMA_CHANNEL_SW, UDMA_ATTR_ALL );
    uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						  UDMA_SIZE_16 | UDMA_SRC_INC_NONE |
						  UDMA_DST_INC_16 | UDMA_ARB_1);


	uDMAChannelAttributeDisable(UDMA_CHANNEL_SW, UDMA_ATTR_ALL );
	uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
				UDMA_MODE_AUTO,
				g_ulADCValues+UDMA_SIZE*(UDMA_BLOCKS-1),
							   g_ulADCValues, UDMA_SIZE);

	ROM_uDMAChannelEnable(UDMA_CHANNEL_SW);
	//ROM_uDMAChannelRequest(UDMA_CHANNEL_SW);
}

