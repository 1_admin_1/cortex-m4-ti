/*
 * main.c
 */
#define TARGET_IS_BLIZZARD_RB1

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "utils/ustdlib.h"
#include "inc/hw_adc.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_gpio.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"


#include "driverlib/cpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "driverlib/udma.h"
#include "driverlib/fpu.h"
#include "driverlib/pin_map.h"
//#include "driverlib/uart.h"
#include "utils/uartstdio.h"

#include "driverlib/rom.h"
#include "lcd44780_LP.h"
#include "adcudma.h"
#include "dsp.h"
#include <math.h>

static volatile int noSignalCount=0;

void InitBasics(void)
{
	ROM_FPUEnable();
	ROM_FPULazyStackingEnable();
	ROM_SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
					   SYSCTL_OSC_MAIN);

}

void initUART(void)
{
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
	ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
	ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	//UARTStdioInit(0);
	UARTStdioConfig(0, 115200, SysCtlClockGet());
    UARTprintf("Init UART\n");
}


int main(void) {
	InitBasics();

	initUART();
	initLCD();
	InitDSP();
	flag=1;

	ROM_IntPrioritySet(INT_TIMER0A,  0);
	//IntPrioritySet(INT_ADC0SS3,  1);

	LCDCommand(0xA0);
	LCD_BuildCustomCharacters();
	InitADC3Transfer();
	InitSWTransfer();

	IntEnable(INT_ADC0SS3);
	IntEnable(INT_TIMER0A);
	unsigned char oldBlocks=0;
	while(1){
		if(oldBlocks!=readyBlocks)
		{
			oldBlocks=readyBlocks;
			ProcessData(readyBlocks);
			IntTrigger(INT_TIMER0A);
		}
	};

	return 0;
}


void Timer0IntHandler(void)
{
	LCDCommand(0x80);
	int i;
	unsigned char level;
	bool noSound=true;
	for (i=0;i<8;i++)
	{
		if (LEDDisplay[i]!=0)
			noSound=false;
	}
	if (noSound)
	{
		if (noSignalCount++>50)
			LCDWriteText("��� ����",0,0);
	}
	else{
		noSignalCount=0;
		for (i=0;i<8;i++)
		{
			level=LEDDisplay[i];
			if (level>8)
				LCDWrite(level-9);
			else
				LCDWrite(0x20);
		}
	}
		LCDCommand(0xC0);
		for (i=0;i<8;i++)
		{
			level=LEDDisplay[i];
			if (level>8)
				level=8;
			if (level==0)
				LCDWrite(0x20);
			else
				LCDWrite(level-1);
		}


}


