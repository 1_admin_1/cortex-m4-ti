/*
 *       lcd44780_LP.c
 *		Basic HD44780 driver for Stellaris Launchpad
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "lcd44780_LP.h"

const char lcd_epson_rus_codes[64] ={
0x41,0xA0,0x42,0xA1,0xE0,0x45,0xA3,0xA4,
0xA5,0xA6,0x4B,0xA7,0x4D,0x48,0x4F,0xA8,
0x50,0x43,0x54,0xA9,0xAA,0x58,0xE1,0xAB,
0xAC,0xE2,0xAD,0xAE,0xAD,0xAF,0xB0,0xB1,
0x61,0xB2,0xB3,0xB4,0xE3,0x65,0xB6,0xB7,
0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0x6F,0xBE,
0x70,0x63,0xBF,0x79,0xE4,0x78,0xE5,0xC0,
0xC1,0xE6,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7
};

void initLCD() {

	SysCtlPeripheralEnable(LCDPORTENABLE);
	GPIOPinTypeGPIOOutput(LCDPORT,
				0xff);

	// Please refer to the HD44780 datasheet for how these initializations work!
	SysCtlDelay((500e-3)*CLKSPEED/3);

	GPIOPinWrite(LCDPORT, RS,  0x00 );

	GPIOPinWrite(LCDPORT, D4 | D5 | D6 | D7,  0x30 );
	GPIOPinWrite(LCDPORT, E, 0x02);SysCtlDelay((20e-6)*CLKSPEED/3); GPIOPinWrite(LCDPORT, E, 0x00);

	SysCtlDelay((50e-3)*CLKSPEED/3);

	GPIOPinWrite(LCDPORT, D4 | D5 | D6 | D7,  0x30 );
	GPIOPinWrite(LCDPORT, E, 0x02);SysCtlDelay((20e-6)*CLKSPEED/3);GPIOPinWrite(LCDPORT, E, 0x00);

	SysCtlDelay((50e-3)*CLKSPEED/3);

	GPIOPinWrite(LCDPORT, D4 | D5 | D6 | D7,  0x30 );
	GPIOPinWrite(LCDPORT, E, 0x02);SysCtlDelay((20e-6)*CLKSPEED/3); GPIOPinWrite(LCDPORT, E, 0x00);

	SysCtlDelay((10e-3)*CLKSPEED/3);

	GPIOPinWrite(LCDPORT, D4 | D5 | D6 | D7,  0x20 );
	GPIOPinWrite(LCDPORT, E, 0x02);SysCtlDelay((20e-6)*CLKSPEED/3); GPIOPinWrite(LCDPORT, E, 0x00);

	SysCtlDelay((10e-3)*CLKSPEED/3);

	LCDCommand(0x01);	// Clear the screen.
	SysCtlDelay((5e-3)*CLKSPEED/3);
	LCDCommand(0x06);	// Cursor moves right.
	SysCtlDelay((5e-3)*CLKSPEED/3);
	LCDCommand(0x0c);	// Cursor blinking, turn on LCD.
	SysCtlDelay((5e-3)*CLKSPEED/3);


}

void LCDCommand(unsigned char command) {

	GPIOPinWrite(LCDPORT, D4 | D5 | D6 | D7, (command & 0xf0) );
	GPIOPinWrite(LCDPORT, RS, 0x00);
	GPIOPinWrite(LCDPORT, E, 0x02);SysCtlDelay((20e-6)*CLKSPEED/3); GPIOPinWrite(LCDPORT, E, 0x00);

	SysCtlDelay((100e-6)*CLKSPEED/3);

	GPIOPinWrite(LCDPORT, D4 | D5 | D6 | D7, (command & 0x0f) << 4 );
	GPIOPinWrite(LCDPORT, RS, 0x00);
	GPIOPinWrite(LCDPORT, E, 0x02);SysCtlDelay((20e-6)*CLKSPEED/3); GPIOPinWrite(LCDPORT, E, 0x00);

	SysCtlDelay((5e-4)*CLKSPEED/3);

}


void LCDWrite(unsigned char inputData) {

	GPIOPinWrite(LCDPORT, D4 | D5 | D6 | D7, (inputData & 0xf0) );
	GPIOPinWrite(LCDPORT, RS, 0x01);
	GPIOPinWrite(LCDPORT, E, 0x02)
	;SysCtlDelay((20e-6)*CLKSPEED/3);
	GPIOPinWrite(LCDPORT, E, 0x00);
	SysCtlDelay((100e-6)*CLKSPEED/3);

	GPIOPinWrite(LCDPORT, D4 | D5 | D6 | D7, (inputData & 0x0f) << 4 );
	GPIOPinWrite(LCDPORT, RS, 0x01);
	GPIOPinWrite(LCDPORT, E, 0x02);
	SysCtlDelay((20e-6)*CLKSPEED/3);
	GPIOPinWrite(LCDPORT, E, 0x00);
	SysCtlDelay((5e-4)*CLKSPEED/3);

}

unsigned char addressRowCol(unsigned char row,unsigned char col)
{
	unsigned char address_d = 0; // address of the data in the screen.
	switch(row)
	{
	case 0: address_d = 0x80 + col;		// at zeroth row
	break;
	case 1: address_d = 0xC0 + col;		// at first row
	break;
	case 2: address_d = 0x94 + col;		// at second row
	break;
	case 3: address_d = 0xD4 + col;		// at third row
	break;
	default: address_d = 0x80 + col;	// returns to first row if invalid row number is detected
	break;
	}
	return address_d;
}
void LCDWriteText(char* inputText,unsigned char row, unsigned char col) {

	LCDCommand(addressRowCol(row,col));

	while(*inputText)					// Place a string, letter by letter.
		LCDWrite(lcd_epson_rus(*inputText++));
}


void LCDWScrollText(char* inputText,unsigned char row, unsigned char col) {

	LCDCommand(addressRowCol(row,col));
	while(*inputText)					// Place a string, letter by letter.
		LCDWrite(*inputText++);
}

void LCD_BuildCustomCharacters()
{	int i,j;
	for(i=0;i<8;i++){
		unsigned char pattern[8]={0x1f,0x1f,0x1f,0x1f,0x1f,0x1f,0x1f,0x1f};
		for(j=0;j<i;j++){
			pattern[j]=0x0;
		}
		LCD_build(7-i,pattern);
	};
}




void LCD_build(unsigned char location, unsigned char *ptr){
       unsigned char i;
       if(location<8){
    	   LCDCommand(0x40+(location*8));
           for(i=0;i<8;i++)
        	   LCDWrite(ptr[ i ] );
           LCDCommand(0x80);
      }

 }



void LCDWritePos(unsigned char inputData,unsigned char row, unsigned char col) {

	LCDCommand(addressRowCol(row,col));
	GPIOPinWrite(LCDPORT, D4 | D5 | D6 | D7, (inputData & 0xf0) );
	GPIOPinWrite(LCDPORT, RS, 0x01);
	GPIOPinWrite(LCDPORT, E, 0x02);
	SysCtlDelay((20e-6)*CLKSPEED/100);
	GPIOPinWrite(LCDPORT, E, 0x00);

	GPIOPinWrite(LCDPORT, D4 | D5 | D6 | D7, (inputData & 0x0f) << 4 );
	GPIOPinWrite(LCDPORT, RS, 0x01);
	GPIOPinWrite(LCDPORT, E, 0x02);
	GPIOPinWrite(LCDPORT, E, 0x00);

}


inline char lcd_epson_rus(char c)
{
        if (c >= '�') c = lcd_epson_rus_codes[c - '�'];
                else if(c == '�') c = 0xA2;
                        else if(c == '�') c = 0xB5;
        return c;
}

void LCDScrollLeft(){
	LCDCommand(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
	}


void LCDScrollRight(){
	LCDCommand(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
	}




