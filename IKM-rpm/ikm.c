/*
 * ikm.c
 *
 *  Created on: 21 ���. 2014 �.
 *      Author: 1_admin_1
 */
#include <stdbool.h>
#include "ikm.h"
#include "adcudma.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_gpio.h"
#define DWT_O_CYCCNT 0x00000004
#define uint32_t unsigned int
static        unsigned int                  c_start, c_stop;

void EnableTiming(void){
static int enabled = 0;
if (!enabled){
   HWREG(NVIC_DBG_INT) |= 0x01000000;            /*enable TRCENA bit in NVIC_DBG_INT*/
   HWREG(DWT_BASE + DWT_O_CYCCNT) = 0;  /* reset the counter */
   HWREG(DWT_BASE) |= 0x01;                                /* enable the counter */
   enabled = 1;
 }
}

bool MNK(unsigned short *pData,unsigned int firstIndexLine, unsigned int lastIndexLine,float *a, float *b)
{	float n=0;
	float sx=0, sx2=0, sy=0, sxy=0;
	float dtime, dvalue;
	dtime=0;
	while(firstIndexLine<=lastIndexLine)
	{
		dvalue=*(pData+firstIndexLine);
		sx+=dtime;
		sy+=dvalue;
		sx2+=dtime*dtime;
		sxy+=dtime*dvalue;
		firstIndexLine+=1;
		dtime+=1;
		n++;
	}
	float D = n * sx2 - sx * sx;
	if (D != 0)
	{
		*b = (float)(sy * sx2 - sx * sxy)/D;
		*a = (float)(n * sxy - sx * sy)/D;
		return 1;
	}
	else return 0;
}

float FindMarksChannel(unsigned short * pData)
{
	EnableTiming();
	c_start = HWREG(DWT_BASE + DWT_O_CYCCNT); // at the beginning of the code

		float udTime[NUM_MARKS*2+6];
		bool allPoint=false;
		unsigned int udTimeSize=0;
		float pTimeMarksCh[NUM_MARKS+2];
		unsigned int pTimeMarksChSize=0;
		unsigned short maxlocal=0;
		unsigned short	minlocal=10000;
		float cropmax=0, cropmin=0;
		unsigned short d1=0,d2=0,d=0;
		unsigned int minnum=0, maxnum=0;
		unsigned int firstIndexLine=0,lastIndexLine=0;
		unsigned int addAddress;
		bool noFirst=false;
		unsigned int numPoint=0;
		unsigned int index=0, i=300;
		float x=0, y=0;
		while(UDMA_BUFFER_SIZE>(addAddress=i))
		{
			d = *(pData+addAddress);
			if(d>maxlocal && d1-d2>d-d1)
			{
				maxlocal=d; index=i; maxnum+=1;
			}
			if (d<minlocal && d1-d<d2-d1)
			{
				 minlocal=d; index=i; minnum+=1;
			}
			d2=d1;
			d1=d;
			if (maxnum>minnum && maxlocal-d1>LEVEL)
			{
				if(noFirst){
					cropmin=minlocal+(maxlocal-minlocal)*CROP1;
					cropmax=maxlocal-(maxlocal-minlocal)*CROP2;
					while (index>300)
					{
						addAddress=index;
						d = *(pData+addAddress);
						if(d>cropmax) lastIndexLine=addAddress;
						else if(d<cropmin)
						{
							firstIndexLine=addAddress;
							break;
						}
						index--;
					}
					if(MNK(pData,firstIndexLine,lastIndexLine,&x,&y))
					{
						udTime[udTimeSize++]=(firstIndexLine+(((cropmax+cropmin)/2.0)-y)/x);
						if(udTimeSize>=NUM_MARKS*2+4)
						{
							break;
						}
					}

				}
				else if(maxnum>10) noFirst=true;
				minlocal=10000;	 minnum=0; maxnum=0;
			}
			if (maxnum<minnum && d1-minlocal>LEVEL)
			{
			 	if(noFirst)
			 	{
					cropmin=minlocal+(maxlocal-minlocal)*CROP1;
					cropmax=maxlocal-(maxlocal-minlocal)*CROP2;
					while (index>300)
					{
						addAddress=index;
						d = *(pData+addAddress);
						if(d<cropmin) lastIndexLine=addAddress;
						else if(d>cropmax)
						{
							firstIndexLine=addAddress;
							break;
						}
						index--;
					}
					if(allPoint||numPoint==0){
					if(MNK(pData,firstIndexLine,lastIndexLine,&x,&y))
					{
						udTime[udTimeSize++]=(firstIndexLine+(((cropmax+cropmin)/2.0)-y)/x);
						if(udTimeSize>=NUM_MARKS*2+4)
						{
							break;
						}
					}
				}}

				minnum=0; maxnum=0;maxlocal=0;
			}
			i++;
		};
	unsigned int jj=0;
	if(udTimeSize>=NUM_MARKS*2+4)
	{

		while(jj<udTimeSize/2)
		{
			pTimeMarksCh[jj++] =((udTime[2*jj+1]+udTime[2*jj])/2.0);
		}
	}
	if(jj>NUM_MARKS)
	{
		c_stop = HWREG(DWT_BASE + DWT_O_CYCCNT); // at the end of the code
		UARTprintf("%u\n",c_stop-c_start);

	return (pTimeMarksCh[NUM_MARKS]-pTimeMarksCh[0]);
	}
	return -1;
}
