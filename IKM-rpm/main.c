/*
 * main.c
 */
#define TARGET_IS_BLIZZARD_RB1

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "utils/ustdlib.h"
#include "inc/hw_adc.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_gpio.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"


#include "driverlib/cpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "driverlib/udma.h"
#include "driverlib/fpu.h"
#include "driverlib/pin_map.h"
//#include "driverlib/uart.h"
#include "utils/uartstdio.h"

#include "driverlib/rom.h"
#include "lcd44780_LP.h"
#include "adcudma.h"
#include <math.h>
#include "ikm.h"
float fi;
char charRev[7];
//int a;
volatile bool buttonCheck;

void InitBasics(void)
{
	ROM_FPUEnable();
	ROM_FPULazyStackingEnable();

	ROM_SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
					   SYSCTL_OSC_MAIN);

}

void initUART(void)
{
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
	ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
	ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	//UARTStdioInit(0);
	UARTStdioConfig(0, 115200, SysCtlClockGet());
	//
	// Hello!
	//
    UARTprintf("Init UART\n");
}

void initButton()
{
	buttonCheck=false;
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Enable pin PF0/4 for GPIOInput
    //First open the lock and select the bits we want to modify in the GPIO commit register.;
	HWREG(GPIO_PORTF_BASE+GPIO_O_LOCK)=GPIO_LOCK_KEY;
	HWREG(GPIO_PORTF_BASE + GPIO_O_CR)=0xFF;
	HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK)=0x0;
	GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_0);
	GPIOPadConfigSet(GPIO_PORTF_BASE,GPIO_PIN_0,GPIO_STRENGTH_8MA || GPIO_DISCRETE_INT, GPIO_PIN_TYPE_STD);
    IntEnable(INT_GPIOF);

    GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_0, GPIO_FALLING_EDGE);
	GPIOIntEnable(GPIO_PORTF_BASE, GPIO_PIN_0);


}
void GPIOFIntHandler()
{
	GPIOIntClear(GPIO_PORTF_BASE, GPIO_PIN_0);
	IntPendClear(INT_GPIOF);
	buttonCheck=!buttonCheck;
	LCDCommand(0x01);
	SysCtlDelay((5e-4)*CLKSPEED/3);
	LCDCommand(0x06);
	SysCtlDelay((5e-4)*CLKSPEED/3);
	if(!buttonCheck)
		LCDWriteText("��/���",1,2);
	SysCtlDelay((5e-4)*CLKSPEED/3);
}


void LCDRev(float fRev){
	char charRev[6];
	static float oldRev=0;
	sprintf(charRev, "%f", fRev);
	//int iRev;
	//iRev=fRev;
	//snprintf(charRev, sizeof(charRev), "%i", iRev);
	LCDWriteText(charRev,0,2);
	LCDCommand(0xC0);
	LCDWrite(4);
	unsigned char bar=(unsigned char)(fRev/500);
	if (bar>7) bar=7;
	if (fRev==0) bar=0x20;
	LCDCommand(0xC0);
	LCDWrite(bar);
	LCDCommand(0x80);
	if(oldRev<fRev)
		LCDWrite(0xd9);
	else
		LCDWrite(0xda);
	oldRev=fRev;
}

void LCDMaxMin(unsigned short maxV, unsigned short minV)
{
	char charRev[6];
	LCDWriteText("Max",0,0);
	LCDWriteText("Min",1,0);
	sprintf(charRev, "%u", maxV);
	LCDWriteText(charRev,0,4);
	sprintf(charRev, "%u", minV);
	LCDWriteText(charRev,1,4);
	if (minV<100)
		LCDWriteText("  ", 1, 6);
	else if (minV<1000)
		LCDWriteText(" ", 1, 7);

}


int main(void) {
	InitBasics();

	initUART();
	initLCD();
	initButton();
	flag=1;


	LCDCommand(0xA0);
	LCD_BuildCustomCharacters();
	int i;

	LCDWriteText("��/���",1,2);
	LCDCommand(0x80);
	fi=10;
	InitADC3Transfer();
	IntEnable(INT_ADC0SS3);

	while(1){
		if(readyData)
		{
			readyData=false;
			/*for (i=0;i<8000;i++)
			{
				g_ulADCValues[i]=2048*sin(3.1415926*(float)i/0.0)+2048;
			}
			*/
			if (buttonCheck)
			{
				unsigned short minVal=10000;
				unsigned short maxVal=0;
				for (i=0;i<UDMA_BUFFER_SIZE;i++)
				{
					unsigned short v=g_ulADCValues[i];
					if (v<minVal)
						minVal=v;
					if (v>maxVal)
						maxVal=v;
				}
				LCDMaxMin(maxVal,minVal);
			}
			else{
				fi=FindMarksChannel(g_ulADCValues);
				if (fi==-1)
					LCDWriteText(" ���  ",0,2);
				else
				LCDRev(125000.0/fi*60.0);
			}

			startUDMA();
			ADCIntEnable(ADC0_BASE, ADC_SEQUENCER);
		}

	};

	return 0;
}

