/*
 * main.c
 */
#define TARGET_IS_BLIZZARD_RB1

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "utils/ustdlib.h"
#include "inc/hw_adc.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_gpio.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"


#include "driverlib/cpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "driverlib/udma.h"
#include "driverlib/fpu.h"
#include "driverlib/pin_map.h"
//#include "driverlib/uart.h"
#include "utils/uartstdio.h"

#include "driverlib/rom.h"
#include "adcudma.h"
#include <math.h>
#define PORTB  		 GPIO_PORTB_BASE
#define PORTD  		 GPIO_PORTD_BASE
#define PORTF  		 GPIO_PORTF_BASE
#define PORTC  		 GPIO_PORTC_BASE
#define PORTA  		 GPIO_PORTA_BASE
#define PORTE  		 GPIO_PORTE_BASE
#define CLKSPEED		 40000000
#define P0 				 GPIO_PIN_0
#define P1 				 GPIO_PIN_1
#define P2				 GPIO_PIN_2
#define P3 				 GPIO_PIN_3
#define P4 				 GPIO_PIN_4
#define P5 				 GPIO_PIN_5
#define P6				 GPIO_PIN_6
#define P7 				 GPIO_PIN_7

float fi;
char charRev[7];
//int a;
volatile bool statusWork;
uint8_t angleSensor;
bool fastMethod;
void colorLED(uint8_t color)
{
	int i=0;
	for(;i<5000;i++)
	{
	GPIOPinWrite(PORTF, P1|P2|P3,color);
	SysCtlDelay(CLKSPEED/2000000);
	GPIOPinWrite(PORTF, P1|P2|P3,0x0);

	SysCtlDelay(CLKSPEED/50000);
	}
}
void status(char *ch)
{
	UARTprintf("[%2i] %s\n",angleSensor,ch);
}

void InitBasics(void)
{
	ROM_FPUEnable();
	ROM_FPULazyStackingEnable();

	ROM_SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
					   SYSCTL_OSC_MAIN);

}

void initUART(void)
{
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
	ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
	ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, P0 | P1);
	//UARTStdioInit(0);
	UARTStdioConfig(0, 115200, SysCtlClockGet());


    UARTprintf("Init UART\n");
}

void initButton()
{
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	HWREG(PORTF + GPIO_O_LOCK)=GPIO_LOCK_KEY;
	HWREG(PORTF + GPIO_O_CR)=0x01;
	HWREG(PORTF + GPIO_O_LOCK)=0x0;
	GPIOPinTypeGPIOInput(PORTF, P0|P4);
	GPIOPinTypeGPIOOutput(PORTF, P1|P2|P3);
	GPIOPadConfigSet(PORTF,P0|P4,GPIO_STRENGTH_4MA, GPIO_PIN_TYPE_STD_WPU);
	IntEnable(INT_GPIOF);
    GPIOIntTypeSet(PORTF, P0|P4, GPIO_FALLING_EDGE);
	GPIOIntEnable(PORTF, P0|P4);
	statusWork=false;

}
void GPIOFIntHandler()
{
	//IntPendClear(INT_GPIOF);
	GPIOIntClear(PORTF, P0|P4);
	if (!GPIOPinRead(PORTF, P4))
		statusWork=true;
	if (!GPIOPinRead(PORTF, P0))
	{
		statusWork=false;
		colorLED(0x2);
	}
}


void stepMotor(uint8_t angle)
{
	colorLED(0x2);
	uint8_t pin1=angle/2;
	uint8_t port=0x01<<pin1;
	if (pin1%2==1)
	{
		if(++pin1>3)
			pin1=0;
		port=port+(0x01<<(pin1));
	}
	GPIOPinWrite(PORTD, 0x0F,  port );
	UARTprintf("-->Set Angle Sensor: %i\n",angleSensor);
	SysCtlDelay(CLKSPEED/30);
	colorLED(0x6);
}



void initGPIO()
{
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	SysCtlDelay(CLKSPEED/30);
	GPIOPinTypeGPIOInput(PORTB, P1|P5);
	SysCtlDelay(CLKSPEED/30);
	GPIOPadConfigSet(PORTB, P1|P5, GPIO_STRENGTH_4MA, GPIO_PIN_TYPE_STD_WPU);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	SysCtlDelay(CLKSPEED/30);
	GPIOPinTypeGPIOOutput(PORTE, P1|P2|P3); //port 0-4
	SysCtlDelay(CLKSPEED/30);
	GPIOPinWrite(PORTE, P1|P2|P3,0x00);
	SysCtlDelay(CLKSPEED/30);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	SysCtlDelay(CLKSPEED/30);
	GPIOPinTypeGPIOOutput(PORTD, 0xFF);
	SysCtlDelay(CLKSPEED/30);
	GPIOPinWrite(PORTD, 0xFF,0x00);
	SysCtlDelay(CLKSPEED/30);

}

void initAngleStepMotor()
{
	uint8_t i=0;
	for (i=0; i<4;i++)
	{
		stepMotor(2*i);
	}
	for (i=3; i>0;i--)
	{
		stepMotor((i-1)*2);
	}

}

void controlRange(uint8_t pins, bool press)
{
	int i=0;
	while(i<10){
		SysCtlDelay(2*CLKSPEED/30);
		bool result= GPIOPinRead(PORTB, pins)==pins;
		if(result!=press) i++;
		else i=0;
	};
}

void driveEngine()
{
	status("Drive engine");
	GPIOPinWrite(PORTE, P2,0x0); //������ �����������
	SysCtlDelay(2*CLKSPEED/3);
	GPIOPinWrite(PORTE, P1,P1);	//������ ���������
	SysCtlDelay(2*CLKSPEED/3);
	controlRange(P1,true);		//�������� ��������� ������� � ������� �������
	GPIOPinWrite(PORTE, P3,P3); //�������� ��� � ������� �������
	status("Working range");
	controlRange(P5,false);		//�������� ������ ������� � �������� �������
	GPIOPinWrite(PORTE, P3,0x0);//�������� ��� � ���������� �������� �������
	status("Range out");
	SysCtlDelay(6*CLKSPEED/3);
	GPIOPinWrite(PORTE, P1,0x0);//������������� ���������
	status("Stop engine");
	SysCtlDelay(3*CLKSPEED/3);
}


void reverseEngine()
{
	if(!GPIOPinRead(PORTB, P1))
	{
	status("Reverse engine");
	GPIOPinWrite(PORTE, P2,P2);	//�������� ����������� �������� ���������
	SysCtlDelay(2*CLKSPEED/3);
	GPIOPinWrite(PORTE, P1, P1);//������ ���������
	SysCtlDelay(2*CLKSPEED/3);
	controlRange(P5,true);		//�������� ��������� ������� �� ������� �������
	GPIOPinWrite(PORTE, P3,P3);	//�������� ��� � ������� �������
	status("Working range");
	controlRange(P1,false);		//�������� ������ ������� � �������� �������
	GPIOPinWrite(PORTE, P3,0x0);//�������� ��� � ���������� �������� �������
	status("Range out");
	SysCtlDelay(6*CLKSPEED/3);
	GPIOPinWrite(PORTE, P1,0x0);//��������� ���������
	status("Stop engine");
	SysCtlDelay(3*CLKSPEED/3);
	}
}

void getUARTAngle()
{
	uint8_t setIntAngle=0;
	fastMethod=false;
	UARTprintf("Use fast Method (Y/N)? ");
	while(1)
	{	if (UARTCharsAvail(UART0_BASE))
		{
			char answer=UARTCharGet(UART0_BASE);
			if (answer=='Y'||answer=='y')
			{
				fastMethod=true;
				UARTprintf("Set Fast Method\n");
			}
			else
				UARTprintf("Set Slow Method\n");
			break;
		}
	}

	UARTprintf("Enter angle for start: ");
	while(1)
	{
		if (UARTCharsAvail(UART0_BASE))
		{
			char setNumAngle=UARTCharGet(UART0_BASE);
			if (((uint8_t)setNumAngle>='0'&&(uint8_t)setNumAngle<='9')||setNumAngle=='\r'||setNumAngle=='\n')
			{
			UARTCharPut(UART0_BASE,setNumAngle);
			if (setNumAngle=='\r'||setNumAngle=='\n')
			{
				UARTprintf('\r\n');
				while(angleSensor<setIntAngle)
					stepMotor(++angleSensor%8);
				break;
			}
			setIntAngle = (uint8_t)(setNumAngle - '0')+setIntAngle*10;
			}
		}
}
}
int main(void) {
	InitBasics();
	initUART();
	initButton();
	GPIOPinWrite(PORTF, P1|P2|P3,P1|P2|P3);
	SysCtlDelay(4*CLKSPEED/3);
	initGPIO();
	initAngleStepMotor();
	//GPIOPinWrite(PORTA, 0x0E,  0x0E );
	reverseEngine();
	status("Ready");
	while(1){
		colorLED(0x8);
		if (statusWork)
		{
			getUARTAngle();
			status("Start");
			driveEngine();
			reverseEngine();
			stepMotor(++angleSensor%8);
			while(statusWork)
			{
				driveEngine();
				if (fastMethod)
						stepMotor(++angleSensor%8);
				reverseEngine();
				stepMotor(++angleSensor%8);
			};
			while(angleSensor>0)
			{
				stepMotor(--angleSensor%8);
			};
		}
	};

	return 0;
}

